
let init = () => {

    let data = []

    fetch("https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json")
        .then(fet => fet.json())
        .then(da => data.push(...da));

    let searchBar = document.querySelector("#searchBar");
    let list = document.querySelector(".u-list");

    searchBar.addEventListener("change",()=>{
        let posMatches = findMatches(searchBar.value);
        if(posMatches){makeMatch(posMatches,searchBar.value);}
    });

    searchBar.addEventListener("keyup",()=>{
        let posMatches = findMatches(searchBar.value);
        if(posMatches){makeMatch(posMatches,searchBar.value);}

    });

    let regexWord = "";

    let findMatches = word => {
        regexWord = new RegExp(word,'gi');
        console.log(regexWord);
        if(regexWord == "/(?:)/gi")
        {
            list.innerHTML = "";
            return 0;
        }
        return data.filter(oneArray => {
            return oneArray.city.match(regexWord) || oneArray.state.match(regexWord);
        })
    };

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    let makeMatch = (matchs,searched) =>{
        let html = "";
        matchs.forEach(cur => html += postUI(cur,searched));
        list.innerHTML = html;
    }
    let postUI = (word,searched) => {
        let cityF = word.city.replace(regexWord,`<span class="highlighted">${searched}</span>`)
        let stateF = word.state.replace(regexWord,`<span class="highlighted">${searched}</span>`)
        let populationF = numberWithCommas(word.population);
        let html = `<li class="u-list-li">
                        <span class="cityState">${cityF}, ${stateF}</span>
                        <span class="population">${populationF}</span>
                    </li>`
        return html;
    };
};

init();

