let timeNow = new Date();
(function(){
    setInterval(function(){
        timeNow = new Date();
        let seconds = timeNow.getSeconds();
        let minutes = timeNow.getMinutes();
        let hours = timeNow.getHours();
        // console.log(seconds,minutes,hours); 
        let degreeSeconds = seconds * 6;
        let degreeMinutes = minutes * 6;
        let degreeHours = hours * 30;
        document.querySelector(".second").style.transform = `rotate(${degreeSeconds-90}deg)`; 
        document.querySelector(".minute").style.transform = `rotate(${degreeMinutes-90}deg)`; 
        document.querySelector(".hour").style.transform = `rotate(${degreeHours-90}deg)`; 
    },1000);
})();
